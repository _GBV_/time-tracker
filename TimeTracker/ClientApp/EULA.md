# End User License Agreement (EULA)

## 1. General Provisions

This End User License Agreement ("Agreement") is a binding agreement between Bohdan Hoholiuk ("Licensor") and the end user ("User") of Time Tracker ("Software"). By installing, copying, or otherwise using the Software, the User agrees to be bound by the terms and conditions of this Agreement.

## 2. License Grant and Restrictions

### 2.1 License Grant
Licensor grants the User a non-exclusive, non-transferable, limited license to use the Software in accordance with the following terms.

### 2.2 Permitted Use
The User may use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

### 2.3 Restrictions
The User shall not:
- Use the Software for any commercial purposes without obtaining a separate agreement with Licensor.
- Reverse engineer, decompile, or disassemble the Software, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation.
- Remove or alter any proprietary notices, labels, or marks from the Software.

## 3. Disclaimer of Warranties and Limitation of Liability

### 3.1 Disclaimer of Warranties
The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. In no event shall the authors or copyright holders be liable for any claim, damages, or other liability, whether in an action of contract, tort, or otherwise, arising from, out of, or in connection with the Software or the use or other dealings in the Software.

### 3.2 Limitation of Liability
In no event shall Licensor be liable for any special, incidental, indirect, or consequential damages of any kind, or any damages whatsoever resulting from loss of use, data, or profits, whether or not advised of the possibility of damage, and on any theory of liability, arising out of or in connection with the use or performance of this Software.

## 4. GDPR Compliance

If the User is located in the European Economic Area (EEA), Licensor shall comply with the General Data Protection Regulation (GDPR) regarding the collection, use, and retention of personal data. Users have the right to access, correct, delete, and restrict the processing of their personal data as described in the Privacy Policy.

## 5. Miscellaneous

### 5.1 Termination
This Agreement is effective until terminated by either party. The User may terminate this Agreement at any time by uninstalling and destroying all copies of the Software. Licensor may terminate this Agreement if the User fails to comply with any terms and conditions of this Agreement.

### 5.2 Governing Law
This Agreement shall be governed by and construed in accordance with the laws of Ukraine, without regard to its conflict of law provisions.
