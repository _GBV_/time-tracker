import React from 'react';
import { NavigationLink } from './NavigationLink';
import { faPaperPlane, faClock, faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { routes } from '../../behavior/routing';

export default {
  title: 'Navigation link',
  component: NavigationLink,
  argTypes: {
    to: {
      type: 'string',
      description: 'The target route for the navigation link',
    },
    icon: {
      description: 'The icon displayed on the navigation link',
    },
    text: {
      type: 'string',
      description: 'The text displayed on the navigation link',
    },
    activeRoutes: {
      type: 'array',
      description: 'Active routes',
    },
  },
};

const Template = (args: any) => <NavigationLink {...args} />;

export const WorktimeNavigationLink = Template.bind({});
WorktimeNavigationLink.arguments = {
  to: routes.worktime,
  icon: faClock,
  text: 'Worktime',
  activeRoutes: [routes.worktime],
};

export const ApprovalsNavigationLink = Template.bind({});
ApprovalsNavigationLink.arguments = {
  to: routes.approvals,
  icon: faCheckCircle,
  text: 'Approvals',
  activeRoutes: [routes.approvals],
};

export const DaysOffNavigationLink = Template.bind({});
DaysOffNavigationLink.arguments = {
  to: routes.daysoff,
  icon: faPaperPlane,
  text: 'Days off',
  activeRoutes: [routes.daysoff],
};
