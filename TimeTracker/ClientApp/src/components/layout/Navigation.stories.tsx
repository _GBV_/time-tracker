import React from 'react';
import { Navigation } from './Navigation';

export default {
  title: 'Button',
  component: Navigation,
};

const Template = () => <Navigation />;

export const NavigationBlock = Template.bind({});
