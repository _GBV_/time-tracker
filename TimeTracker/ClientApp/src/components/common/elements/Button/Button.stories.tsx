import React from 'react';
import { Button } from './Button';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    title: {
      type: 'string',
      description: 'Title for button',
      defaultValue: 'Button',
    },
    size: {
      type: 'string',
      description: 'Button size',
      defaultValue: 'sm',
      options: ['sm', 'lg'],
    },
  },
};

const Template = (args: any) => <Button {...args} />;

export const SmallButton = Template.bind({});
SmallButton.arguments = {
  title: 'Small button',
  size: 'sm',
};

export const LargeButton = Template.bind({});
LargeButton.arguments = {
  title: 'Large button',
  size: 'lg',
};
