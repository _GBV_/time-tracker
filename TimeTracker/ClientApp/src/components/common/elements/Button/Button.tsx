import React from 'react';

type Props = {
  title: string;
  size?: 'sm' | 'lg';
}

export const Button = ({ title, size }: Props) => {
  const sizeClass = size ? `btn-${size}` : '';
  
  return (
    <button className={`btn btn-primary ${sizeClass}`}>
      {title}
    </button>
  );
};
