import React from 'react';
import { SortIcon } from './SortIcon';

export default {
  title: 'Sort icon',
  component: SortIcon,
  argTypes: {
    sortingOrder: {
      type: 'string',
      description: 'Order for sorting',
      defaultValue: 'ASCENDING',
      options: ['ASCENDING', 'DESCENDING'],
    },
  },
};

const Template = (args: any) => <SortIcon {...args} />;

export const AscendingSortIcon = Template.bind({});
AscendingSortIcon.arguments = {
  sortingOrder: 'ASCENDING',
};

export const DescendingSortIcon = Template.bind({});
DescendingSortIcon.arguments = {
  sortingOrder: 'DESCENDING',
};
