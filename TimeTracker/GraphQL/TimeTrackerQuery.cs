﻿using GraphQL;
using GraphQL.Types;
using TimeTracker.GraphQL.Approvals;
using TimeTracker.GraphQL.Calendar;
using TimeTracker.GraphQL.DaysOff;
using TimeTracker.GraphQL.Profile;
using TimeTracker.GraphQL.Users;
using TimeTracker.GraphQL.Worktime;

namespace TimeTracker.GraphQL
{
    public class TimeTrackerQuery : ObjectGraphType
    {
        public TimeTrackerQuery()
        {
            Field<UsersQuery>("Users")
                .Description("Queries to deal with user information and lists of users.")
                .Authorize()
                .Resolve(context => new { });

            Field<ProfileQuery>("Profile")
                .Description("Queries to deal with user profile details.")
                .Resolve(context => new { });
            
            Field<WorktimeQuery>("Worktime")
                .Description("Queries to deal with worktime tracking.")
                .Resolve(context => new { });

            Field<DaysOffQuery>("DaysOff")
                .Description("Queries to deal with vacations and leave days.")
                .Authorize()
                .Resolve(context => new { });

            Field<ApprovalQuery>("Approvals")
                .Description("Queries to deal with approvals.")
                .Resolve(context => new { });

            Field<CalendarQuery>("Calendar")
                .Description("Queries to deal with events, schedules, and calendar entries.")
                .Resolve(context => new { });
        }
    }
}
