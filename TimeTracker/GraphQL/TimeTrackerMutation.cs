﻿using GraphQL;
using GraphQL.Types;
using TimeTracker.GraphQL.Approvals;
using TimeTracker.GraphQL.Calendar;
using TimeTracker.GraphQL.DaysOff;
using TimeTracker.GraphQL.Profile;
using TimeTracker.GraphQL.Users;
using TimeTracker.GraphQL.Worktime;

namespace TimeTracker.GraphQL
{
    public class TimeTrackerMutation : ObjectGraphType
    {
        public TimeTrackerMutation()
        {
            Field<UsersMutation>("Users")
                .Description("Mutations to deal with user information.")
                .Resolve(context => new { });

            Field<ProfileMutation>("Profile")
                .Description("Mutations to deal with user profile actions.")
                .AllowAnonymous()
                .Resolve(context => new { });

            Field<WorktimeMutation>("Worktime")
                .Description("Mutations to deal with worktime tracking actions.")
                .Resolve(context => new { });

            Field<DaysOffMutation>("DaysOff")
                .Description("Mutations to deal with vacation and leave day actions.")
                .Resolve(context => new { });

            Field<ApprovalMutation>("Approvals")
                .Description("Mutations to deal with approval processes.")
                .Resolve(context => new { });

            Field<CalendarMutation>("Calendar")
                .Description("Mutations to deal with calendar events and schedules.")
                .Resolve(context => new { });
        }
    }
}
